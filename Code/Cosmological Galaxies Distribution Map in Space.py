#!/usr/bin/env python
# coding: utf-8

# # Project- Cosmological Galaxies Distribution Map in Space
# ## By- Aarush Kumar
# ### Dated: June 26,2022

# In[1]:


import numpy as np 
import pandas as pd


# In[2]:


df = pd.read_csv('/home/aarush100616/Downloads/Projects/Cosmological Galaxies Distribution Map /Data/Skyserver_SQL2_27_2018 6_51_39 PM.csv')
df.head()


# In[3]:


dfgal = df.loc[(df["class"]) == 'GALAXY']
dfgal.head()


# Galactic coordinate could be formulated into
# * sin(b)=sin(δNGP) cos(ig)−cos(δ) sin(α−αNGP) sin(ig) 
# * cos(b) cos(l−l0)=cos(δ) cos(α−αNG)
# * cos(b) sin(l−l0)=sin(δ) sin(ig)+cos(δ) sin(α−αNGP) cos(ig)
# ** Known  i=62.6o,αN=282.5o,l0=33.0o

# In[4]:


from astropy import units as u
from astropy.coordinates import SkyCoord
from astropy.cosmology import WMAP9 as cosmo
radec = SkyCoord(ra=dfgal['ra']*u.degree, dec=dfgal['dec']*u.degree, frame='icrs')
#radec.ra.value
#radec.dec.value
galactic = radec.galactic
dfgal['l'] = galactic.l.value
dfgal['b'] = galactic.b.value
r = cosmo.comoving_distance(dfgal['redshift'])
dfgal['distance']= r.value
dfgal.head()


# In[5]:


def cartesian(dist,alpha,delta):
    x = dist*np.cos(np.deg2rad(delta))*np.cos(np.deg2rad(alpha))
    y = dist*np.cos(np.deg2rad(delta))*np.sin(np.deg2rad(alpha))
    z = dist*np.sin(np.deg2rad(delta))
    return x,y,z
cart = cartesian(dfgal['distance'],dfgal['ra'],dfgal['dec'])
dfgal['x_coord'] = cart[0]
dfgal['y_coord'] = cart[1]
dfgal['z_coord'] = cart[2]
dfgal.head()


# ## Plot the Galaxies Location

# In[6]:


import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
fig = plt.figure(figsize=(12,10))
ax = fig.add_subplot(111, projection='3d')
ax.scatter(dfgal['x_coord'],dfgal['y_coord'],dfgal['z_coord'], s = 0.7)
ax.set_xlabel('X (mpc)')
ax.set_ylabel('Y (mpc)')
ax.set_zlabel('Z (mpc)')
ax.set_title('Galactic Distribution from SDSS',fontsize=18)
plt.show()


# In[7]:


fig = plt.figure(figsize=(12,10))
ax = fig.add_subplot(111)
ax.scatter(dfgal['x_coord'],dfgal['y_coord'], s = 0.5)
ax.set_xlabel('X (mpc)')
ax.set_ylabel('Y (mpc)')
ax.set_title('Galactic Distribution from SDSS in X and Y Space',fontsize=18)
plt.show()


# In[8]:


fig = plt.figure(figsize=(12,10))
ax = fig.add_subplot(111)
ax.scatter(dfgal['x_coord'],dfgal['z_coord'], s = 0.5)
ax.set_xlabel('X (mpc)')
ax.set_ylabel('Z (mpc)')
ax.set_title('Galactic Distribution from SDSS in X and Z Space',fontsize=18)
plt.show()


# In[9]:


fig = plt.figure(figsize=(12,10))
ax = fig.add_subplot(111)
ax.scatter(dfgal['y_coord'],dfgal['z_coord'], s = 0.5)
ax.set_xlabel('Y (mpc)')
ax.set_ylabel('Z (mpc)')
ax.set_title('Galactic Distribution from SDSS in Y and Z Space',fontsize=18)
plt.show()


# In[10]:


import seaborn as sb
fig = plt.figure(figsize=(12,10))
sb.distplot(dfgal['redshift'])
plt.title('Redshift Distribution',fontsize=18)
plt.show()
fig = plt.figure(figsize=(12,10))
sb.distplot(dfgal['distance'])
plt.title('Distance Distribution (MPC)',fontsize=18)
plt.show()


# In[11]:


dfgal['redshift'].describe()


# In[12]:


dfgal['distance'].describe()

